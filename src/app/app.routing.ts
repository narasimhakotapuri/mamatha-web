﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';
import { UserDetailsComponent } from "src/app/user-details/user-details.component";
import { ProductuploadComponent } from "src/app/productupload/productupload.component";
import { ProductdetailsComponent } from "src/app/productdetails/productdetails.component";
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';
import { ProductQuickviewComponent } from './product-quickview/product-quickview.component';
import { CategoryAddComponent } from './category-add/category-add.component';
import { MultiuploadComponent } from './multiupload/multiupload.component';
// import { EditimgComponent } from './editimg/editimg.component';
import { FillProductDetailsComponent } from './fill-product-details/fill-product-details.component';
import { AdminComponent } from './admin/admin.component';
//import { SendemailComponent } from "src/app/sendemail/sendemail.component";
import { OrderProgressComponent } from "src/app/order-progress/order-progress.component";
import { EditorComponent } from './editor/editor.component';
const appRoutes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" }, 
   // {path: 'sendmail', component: SendemailComponent},
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'productupload', component: ProductuploadComponent},
    { path: 'admin', component: AdminComponent},
    { path: "products", component: ProductsComponent},
    { path: "cart", component: CartComponent},
    {path: 'multiupload', component: MultiuploadComponent},
    {path: 'orderProgress', component: OrderProgressComponent},
    { path: 'userDetails', component: UserDetailsComponent },
    { path: 'productDetails', component: ProductdetailsComponent },
    // otherwise redirect to home
    { path: 'home', component: HomeComponent},
    { path: 'categoryadd', component: CategoryAddComponent},
    
    {path: 'quickView', component: ProductQuickviewComponent},
    {path: 'fillProductDetails', component: FillProductDetailsComponent},
    {path: 'edit',component:EditorComponent}
   
    
    // { path: 'editImg', component: EditimgComponent}
];

export const routing = RouterModule.forRoot(appRoutes);