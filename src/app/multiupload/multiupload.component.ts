import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multiupload',
  templateUrl: './multiupload.component.html',
  styleUrls: ['./multiupload.component.css']
})
export class MultiuploadComponent {
  urls = [];
  ImagesBase64Data =[];

  onSelectFile(event) {
   this.urls =[];
    if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
  
        for (let i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
                reader.onload = (event : any) => {
                  this.urls.push(event.target.result); 
                //  console.log(this.urls);
                }
               reader.readAsDataURL(event.target.files[i]);
        }
    }
  }
  public addProduct() {
    this.ImagesBase64Data=this.urls;
   console.log(this.ImagesBase64Data);
  }
}
