import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from "src/app/_services";
import { first } from 'rxjs/operators';

import { AuthService } from 'ng4-social-login';
import { SocialUser } from 'ng4-social-login';
import { GoogleLoginProvider, FacebookLoginProvider,LinkedinLoginProvider } from 'ng4-social-login';
import { ProductService } from "src/app/_services/product.service";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
   user: SocialUser;
  cartProductLength: any;
  cartProducts: any;
  bill: any;
  categoryList: any;
  categoryList0:any;
   categoryId: any;
   categoryName:any;
   currentUser:any;
   isAdmin :boolean =false;
  constructor(private router: Router,
              private categoryService: CategoryService,
              private authService: AuthService,
              private productService: ProductService) { }

  ngOnInit() { 
     this.authService.authState.subscribe((user) => {
     // console.log("Before printing");
      this.user = user;
      this.currentUser = user;
      // console.log(this.currentUser);
       //console.log(this.currentUser);
      if(!this.currentUser){
        //this.isAdmin = this.currentUser.isAdmin;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        // console.log(this.currentUser);
        if(this.currentUser){
          this.isAdmin = this.currentUser.isAdmin;
        }
      }
     // console.log("After printing");
    });
      this.initiateData();
      this.loadAllProducts();
    this.cartProductLength = localStorage.getItem('cartProductLength');
 
  }
   signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

   signInWithLinkedin(): void {
    this.authService.signIn(LinkedinLoginProvider.PROVIDER_ID);
  }
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  // signInWithLinkedIn(): void {
  //   this.authService.signIn(LinkedInLoginProvider.PROVIDER_ID);
  // }

  signOut(): void {
    this.authService.signOut();
  }
    initiateData() {
  this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser) {
      this.productService.getAllProductInCartByEmail(this.currentUser.email).pipe(first()).subscribe(products => {
        this.cartProducts = products;
        this.bill = 0;
        // let data = localStorage.getItem('cart');
        // console.log(data);
        if (this.cartProducts) {
          //this.cartProducts = JSON.parse(data);
          for (let i in this.cartProducts) {
            this.cartProducts[i]["qt"] = 1;
            this.bill = this.bill + this.cartProducts[i].price * this.cartProducts[i].qt;
          }
        } else {
          this.cartProducts = [];
        }

      });
    }
  }
  private loadAllProducts() {
    this.categoryService.getAll().pipe(first()).subscribe(products => {
      this.categoryList = products;
    });
  }
 productsInCatagory(catagory) {
    this.categoryId = catagory._id;
    this.categoryName = catagory.catName;
    localStorage.setItem('categoryId', this.categoryId);
    localStorage.setItem('categoryName', this.categoryName);
    location.reload();
    this.router.navigate(['/products']);
  }
  navigateToLoginPage() {
    this.router.navigate(['/login']);
  }
  navigateToRegisterPage(){
    this.router.navigate(['/register']);
  }
  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/home']);
    location.reload();
  }

  goToCart() {
    this.router.navigate(['/cart']);
  }
}
