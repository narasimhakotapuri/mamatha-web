import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Category } from "src/app/_models";
import { CategoryService } from "src/app/_services/category.service";
import { first } from "rxjs/internal/operators/first";
import { AlertService } from "src/app/_services";
import { Router } from "@angular/router";

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent {
    loading = false;
    errorMsg: any;
    private category: Category = new Category();;
    catName:any;
    imgBase64Data:any;
    catDesc:any;
    catPriceRange:any;
    private _opened: boolean = false;

    className="main-panel side_bar";
    constructor(
          private categoryService: CategoryService,
          private router: Router, 
        
          private alertService: AlertService) { }

    open(f){
      if(this._opened){
        this.className="main-panel side_bar";
        this._opened=true;
        f.close();
      }
      else{
        this.className="main-panel";
        this._opened=false;
        f.open();
      }
    }
  
    changeListener($event): void {
      this.readThis($event.target);
    }
  
    readThis(inputValue: any): void {
      // tslint:disable-next-line:prefer-const
      var file: File = inputValue.files[0];
      var idxDot = file.type.lastIndexOf(".") + 1;
      var extFile = file.type.substr(idxDot, file.type.length).toLowerCase();
      if (extFile == "image/jpg" || extFile == "image/jpeg" || extFile == "image/png") {
      if (file.size <= 560000) {
      this.errorMsg = '';
      //this.catName = file.name;
      var myReader: FileReader = new FileReader();
     
      myReader.onloadend = (e) => {
      this.imgBase64Data = myReader.result;
      }
      myReader.readAsDataURL(file);
      }
      else {
      this.errorMsg = 'Image size should be less than 70kb';
      this.alertService.error(this.errorMsg, true);
      }
      } else {
      
      this.errorMsg = "Only jpg/jpeg and png files are allowed!";
      this.alertService.error(this.errorMsg, true);
      }
      }
  
    public addCategory() {
      this.category.catName = this.catName;
      this.category.catImgData = this.imgBase64Data;
      this.category.catPriceRange = this.catPriceRange;
      this.category.catDesc = this.catDesc;
      this.loading = true;
      this.categoryService.addCategory(this.category).pipe(first()).subscribe(
        data => {
          this.alertService.success('Category added successful', true);
          this.router.navigate(['/productDetails']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    }
}
