export  class Category {
    id:string;
    catName: string;
    catDesc:string;
    catImgData: string;
    catPriceRange:string;
}