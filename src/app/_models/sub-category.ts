export class SubCategory{
    category_id: string;
    sub_category_name: string;
    sub_category_image: string;
    created_at: string;
    updated_at: string;
}