
export  class Addresses {
            firstName:string;
            lastName:string;
            company:string;
            pincode:number;
            addressLine1:string;
            addressLine2:string;
            city:string;
            state:string;
            country:string;
            phone:string;
            orderMode:string;
}
export class UserOrderDetails {
     email:string;
     paymentMode:string;
     productInCartIds: any;
     addresses: Addresses[];
}

