export  class VisitingCardOrderDetails {
    categoryId:string;
    productId:string;
    comapanyLogoImgName:string;
    comapanyLogoImgBase64Data:any;
    name:string;
    address:string;
    email:string;
    paperType:string;
}