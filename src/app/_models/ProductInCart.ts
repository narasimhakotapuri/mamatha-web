    export  class ProductInCart {
    id: number;
    email:string;
    imgBase64Data: any
    imgName:string;
    desc:string;
    price: number;
    qty:number;
    categoryId:string;
    productInCartId:string;
    oderStatus:boolean;
}
