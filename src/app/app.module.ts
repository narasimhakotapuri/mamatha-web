﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
//import { fakeBackendProvider } from './_helpers';
import { SidebarModule } from 'ng-sidebar';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import {PinchZoomModule} from 'ngx-pinch-zoom';
import { NgxMasonryModule } from 'ngx-masonry';
import { NgMasonryGridModule } from 'ng-masonry-grid';
import { OwlModule } from 'ngx-owl-carousel';
import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService } from './_services';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';;
import { UserDetailsComponent } from './user-details/user-details.component';
import { ProductuploadComponent } from './productupload/productupload.component';
import { ProductdetailsComponent } from './productdetails/productdetails.component';
import { ProductService } from "src/app/_services/product.service";
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { CartComponent } from './cart/cart.component';;
import { HeaderComponent } from './header/header.component'
;
import { FooterComponent } from './footer/footer.component'
;
import { ProductQuickviewComponent } from './product-quickview/product-quickview.component'
;
import { CategoryAddComponent } from './category-add/category-add.component'
import { CategoryService } from './_services/category.service';;
import { VisitingCardOrderDetailsService } from './_services/visitingcardorderdetails.service';;
import { MultiuploadComponent } from './multiupload/multiupload.component'
;
import { FillProductDetailsComponent } from './fill-product-details/fill-product-details.component';;
import { AdminComponent } from './admin/admin.component'
import { SocialLoginModule,AuthServiceConfig,GoogleLoginProvider,FacebookLoginProvider,LinkedinLoginProvider } from 'ng4-social-login';;
//import { SendemailComponent } from './sendemail/sendemail.component'
;
import { OrderProgressComponent } from './order-progress/order-progress.component'
import { EditorComponent } from './editor/editor.component';
// import { EditimgComponent } from './editimg/editimg.component';
const config = new AuthServiceConfig([
  {
    id : GoogleLoginProvider.PROVIDER_ID,
    provider : new GoogleLoginProvider('477578559078-37o5teu38rleum0cq9k197kl3pdj89a8.apps.googleusercontent.com')
  },
   {
    id : FacebookLoginProvider.PROVIDER_ID,
    provider : new FacebookLoginProvider('346598432755263')
  },
   {
    id : LinkedinLoginProvider.PROVIDER_ID,
    provider : new LinkedinLoginProvider('86kwa6qwwzr5ov')
  }

]
,false);

export function provideConfig() {
  return config;
}
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing,
        SocialLoginModule,
        PinchZoomModule,
        NgxMasonryModule,
        NgMasonryGridModule,
        SidebarModule.forRoot(),
        OwlModule
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        UserDetailsComponent ,
        ProductuploadComponent ,
        ProductdetailsComponent ,
        ProductsComponent ,
        CartComponent
,
        HeaderComponent
,
        FooterComponent ,
        ProductQuickviewComponent ,
        CategoryAddComponent ,
        MultiuploadComponent ,
        FillProductDetailsComponent ,
        AdminComponent ,
        //SendemailComponent ,
        OrderProgressComponent, // EditimgComponent,
        EditorComponent
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        ProductService,
        CategoryService,
        VisitingCardOrderDetailsService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        {provide: AuthServiceConfig,useFactory: provideConfig}

        // provider used to create fake backend
        //fakeBackendProvider
    ],
    bootstrap: [AppComponent],
    
})

export class AppModule { }