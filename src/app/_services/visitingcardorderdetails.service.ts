import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { VisitingCardOrderDetails } from '../_models';

@Injectable()
export class VisitingCardOrderDetailsService {
    constructor(private http: HttpClient) { }

    addVisitingCardOrderDetails(visitingcardorderdetails: VisitingCardOrderDetails) {
        //return this.http.post(`${environment.apiUrl}/orderdetails/addVisitingCardOrderDetails`, visitingcardorderdetails);
        return this.http.post(`/orderdetails/addVisitingCardOrderDetails`, visitingcardorderdetails);
    }

}