﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Product } from '../_models';
import { ProductInCart } from "src/app/_models/ProductInCart";

@Injectable()
export class ProductService {
    constructor(private http: HttpClient) { }

    getAll() {
      //return this.http.get<Product[]>(`${environment.apiUrl}/products`);
       return this.http.get<Product[]>(`/products`);
    }
    getAllNewCollections() {
        //return this.http.get<Product[]>(`${environment.apiUrl}/products/getAllNewCollectionProducts`);
         return this.http.get(`https://mamatapress.herokuapp.com/api/card/getNewNormalCards`);
      }
    getAllProductInCartByEmail(email: string) {
       //return this.http.get<ProductInCart[]>(`${environment.apiUrl}/products/email/` + email);
        return this.http.get<ProductInCart[]>(`/products/email/` + email);
    }

     getByCategoryId(id: string) {
        //return this.http.get<Product[]>(`${environment.apiUrl}/products/` + id);
        return this.http.get<Product[]>(`https://mamatapress.herokuapp.com/api/card/getSubCategoryCards/` + id);
      
    }
    addProduct(product: Product) {
        //return this.http.post(`${environment.apiUrl}/products/addProduct`, product);
        return this.http.post(`/products/addProduct`, product);
    }
    addProductInCart(productInCart: ProductInCart) {
        //return this.http.post(`${environment.apiUrl}/products/addProductInCart`, productInCart);
        return this.http.post(`/products/addProductInCart`, productInCart);
       
    }
    // update(user: Product) {
    //     return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
    // }

    deleteCartData(id: string) {
        //return this.http.delete(`${environment.apiUrl}/products/` + id);
        return this.http.delete(`/products/` + id);
    }
}