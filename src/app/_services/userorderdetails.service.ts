import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { UserOrderDetails } from "src/app/_models/user-order-details";

@Injectable({
  providedIn: 'root'
})
export class UserorderdetailsService {

  constructor(private http:HttpClient) { }
    addUserOrderDetails(userOrderDetails: UserOrderDetails) {
        return this.http.post(`/orderdetails/adduserOrderDetails`, userOrderDetails);
        //return this.http.post(`${environment.apiUrl}/orderdetails/adduserOrderDetails`, userOrderDetails);
    }
}
