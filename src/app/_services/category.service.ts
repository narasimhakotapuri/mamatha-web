import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Category } from '../_models';
import { SubCategory } from '../_models/sub-category';

@Injectable()
export class CategoryService {
    constructor(private http: HttpClient) { }

    getAll() {
      return this.http.get<SubCategory[]>("https://mamatapress.herokuapp.com/api/subCategory/getAllSubCategories");
       //return this.http.get<Category[]>(`/categories`);
     
    }
    getDDLValue(){
       return this.http.get<Category[]>(`${environment.apiUrl}/categories/getAllCategoryForddl`);
        //return this.http.get<Category[]>(`/categories/getAllCategoryForddl`);
    }
    getCategoryById(id: number) {
        return this.http.get(`${environment.apiUrl}/categories/` + id);
        //return this.http.get(`/categories/` + id);
    }

    addCategory(category: Category) {
        return this.http.post(`${environment.apiUrl}/categories/addCategory`, category);
        //return this.http.post(`/categories/addCategory`, category);
    }
    getCategory(){
        return this.http.get("https://mamatapress.herokuapp.com/api/subCategory/getAllSubCategories");
    }
}