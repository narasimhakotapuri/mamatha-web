import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../_services/product.service';
import { first } from 'rxjs/operators';
import { CategoryService } from '../_services/category.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {

  products: any;
  cartProducts: any;
  productImg: any;
  productImg1: any;
  cartProductLength: any;
  categoryId: any;
  categoryName: any;

  constructor(private router: Router, private productService: ProductService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.loadAllProducts();
  }


  private loadAllProducts() {
    this.categoryId = localStorage.getItem('categoryId');
    this.categoryName = localStorage.getItem('categoryName');
    this.productService.getByCategoryId(this.categoryId).pipe(first()).subscribe(products => {
      // this.productImg = products;
      this.productImg=Object.assign([],products);
      this.productImg1=Object.assign([],products);
      for(let i=0;i<this.productImg.length;i++){
        this.productImg[i]["place"]=0;
        this.productImg1[i]["place"]=0;
      }
    // console.log("Image is :-"+this.productImg[0].card_images[0].images[0]);
    });
  }
  quickView(product) {
    //console.log('Index', product);
    localStorage.setItem('productId', JSON.stringify(product));
    this.router.navigate(['/quickView']);
  }
  position(index,place){
    

    this.productImg =  JSON.parse(JSON.stringify(this.productImg1));
    this.productImg[index].place=place;
  }
}
