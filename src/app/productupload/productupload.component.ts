import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Product, Category, imgData } from "src/app/_models";
import { ProductService } from "src/app/_services/product.service";
import { CategoryService } from "src/app/_services/category.service";
import { first } from "rxjs/internal/operators/first";
import { AlertService } from "src/app/_services";
import { Router } from "@angular/router";
import { ThrowStmt, analyzeAndValidateNgModules } from '@angular/compiler';
import { observable } from 'rxjs';
import { ColorCode } from '../_models/colorCode';


@Component({
  selector: 'app-productupload',
  templateUrl: './productupload.component.html',
  styleUrls: ['./productupload.component.css']
})

export class ProductuploadComponent implements OnInit {
  loading = false;
  errorMsg: any;

  private product: Product = new Product();
  private imgDataObj: imgData = new imgData();


  imgName: any;
  imgBase64Data: any;
  desc: any;
  price: number;
  categoryId: any;
  categories = [];

  selectedcategory: Category = new Category();//  null;//new Country(2, 'India');
  urlImg = [];

  arrImg = [];
  colorCode = [];
  colorCodeData = [];
  addImageFlag = false;
  public selectedColorCode: ColorCode = new ColorCode("#fffb", "Blue");


  colorCodes = [
    new ColorCode("#FF0000", 'Red'),
    new ColorCode("#006400", 'Dark Green'),
    new ColorCode("#999900", 'Dark Yellow'),
    new ColorCode("#FF1493", 'Deep Pink'),
    new ColorCode("#DBB2D1", 'Pink Lavender')
  ];
  private _opened: boolean = false;

  className="main-panel side_bar";
  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private router: Router,

    private alertService: AlertService) {

  }
  ngOnInit() {
    this.loadAllCategories();

  }
  open(f){
    if(this._opened){
      this.className="main-panel side_bar";
      this._opened=true;
      f.close();
    }
    else{
      this.className="main-panel";
      this._opened=false;
      f.open();
    }
  }
  public onColorSelect(colorCode) {
    this.selectedColorCode = null;
    this.colorCodeData = [];
    for (var i = 0; i < this.colorCodes.length; i++) {
      if (this.colorCodes[i].code == colorCode) {
        this.selectedColorCode = this.colorCodes[i];
        if (localStorage.getItem('colorCodeObj') != null) {
          this.colorCodeData = JSON.parse(localStorage.getItem('colorCodeObj'));
        }
        // Push the new data (whether it be an object or anything else) onto the array
        this.colorCodeData.push(this.selectedColorCode.code);
        // Alert the array value
        // Re-serialize the array back into a string and store it in localStorage
        localStorage.setItem('colorCodeObj', JSON.stringify(this.colorCodeData));
        break;
      }
    }
  }
  public onSelect(cotegoryId) {
    this.selectedcategory = null;
    for (var i = 0; i < this.categories.length; i++) {
      if (this.categories[i].id == cotegoryId) {
        this.selectedcategory = this.categories[i];
        this.categoryId = this.selectedcategory.id;
        break;
      }
    }
  }

  private loadAllCategories() {
    this.categoryService.getDDLValue().pipe(first()).subscribe(categories => {
      this.categories = categories;
    });
  }

  onSelectFile(event) {
    this.urlImg = [];
    this.colorCode = [];
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;

      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          this.imgName = event.target.imgName;
          this.urlImg.push(event.target.result);
          this.SaveDataToLocalStorageImg(this.urlImg);
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }
  /**
   * addToDraft
   */
  public addToDraft() {
    this.addImageFlag = true;
  }

  public SaveDataToLocalStorageImg(data) {
    this.arrImg = [];
    // Parse the serialized data back into an aray of objects
    if (localStorage.getItem('colorCodeImg') != null) {
      this.arrImg = JSON.parse(localStorage.getItem('colorCodeImg'));
    }
    // Push the new data (whether it be an object or anything else) onto the array
    this.arrImg.push(data);
    // Alert the array value
    // alert(this.arrColorCode);  // Should be something like [Object array]
    // Re-serialize the array back into a string and store it in localStorage
    localStorage.setItem('colorCodeImg', JSON.stringify(this.arrImg));
  }
  public addProduct() {
    if (this.price && this.desc && this.imgName) {
    this.imgDataObj.colorCode = JSON.parse(localStorage.getItem('colorCodeObj'));
    this.imgDataObj.imgBase64Data = JSON.parse(localStorage.getItem('colorCodeImg'));
    this.product.imgData = this.imgDataObj;
    this.product.imgName = this.imgName;
    this.product.price = this.price;
    this.product.desc = this.desc;
    this.product.categoryId = this.categoryId;
    this.loading = true;
    this.productService.addProduct(this.product).pipe(first()).subscribe(
      data => {
        this.alertService.success('product added successful', true);
        this.router.navigate(['/productDetails']);
        localStorage.removeItem('colorCodeObj');
        localStorage.removeItem('colorCodeImg');
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
    } else {
      this.errorMsg = 'Please enter all the fields';
    }
  }

  saveImages() {
    if(confirm("If you want to add more Images please click Yes or else click No.")){
    this.addImageFlag = false;
    }else{
    this.addImageFlag = true;
    }

  }
  
}
