﻿import { Component, OnInit,HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { CategoryService } from '../_services/category.service';
import { ProductService } from '../_services/product.service';
declare var jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  message: any;
  categoryList: any;
  categoryId: any;
  categoryName: any;
  newCollectionProducts :any;
  newCollectionProducts1 :any;
  next=3;
  prev=0;
  place=0;
  screenHeight:any;
  screenWidth:any;
  constructor(private router: Router,
     private categoryService: CategoryService,
     private productService: ProductService) { 
      this.getScreenSize();
     }

  ngOnInit() {
    this.message = JSON.parse(localStorage.getItem('currentUser'));
    this.loadAllProducts();

    

    // this.categoryService.getCategory()
    //   .subscribe(
    //     (response)=>{
    //       console.log(response);
    //     }
    //   );
    // jQuery('.carousel').carousel({
    //   interval: 2000
    // });
  }
  // viewProducts() {
  //   this.router.navigate(['/products']);
  // }
  increase(){
    if(this.categoryList.length-1>this.next){
      this.next=this.next+1;
      this.prev=this.prev+1;
      // this.i=0;
      // if(this.next==5);
      console.log(this.next +"  "+this.prev);
    }
    
  }
  decrease(){
    if(this.prev>0){
      this.next=this.next-1;
      this.prev=this.prev-1;
      console.log(this.next +"  "+this.prev);
      // this.i=0;
    }
  }
  position(index,place){
    // for(let i=0;i<this.newCollectionProducts.length;i++){
    //   if(index===i){
    //     this.newCollectionProducts[index].place=place;
    //     console.log(this.newCollectionProducts[index].place);
    //     console.log(this.newCollectionProducts[index]);  
    //     console.log(this.newCollectionProducts[index].place);
    //   }
    //   else{
    //     this.newCollectionProducts[index].place=0;
    //   }
    // }

    this.newCollectionProducts =  JSON.parse(JSON.stringify(this.newCollectionProducts1));
    this.newCollectionProducts[index].place=place;
  }
  productsInCatagory(catagory) {
    this.categoryId = catagory._id;
    this.categoryName = catagory.catName;
    localStorage.setItem('categoryId', this.categoryId);
    this.router.navigate(['/products']);
  }
  newCollectionInProduct(product){
    localStorage.setItem('productId', JSON.stringify(product));
    this.router.navigate(['/quickView']);

  }
  private loadAllProducts() {
    this.categoryService.getAll().pipe(first()).subscribe(products => {
      this.categoryList = products;
    });
    this.productService.getAllNewCollections().pipe(first()).subscribe(newCallectionsproducts => {
      this.newCollectionProducts=Object.assign([],newCallectionsproducts);
      this.newCollectionProducts1=Object.assign([],newCallectionsproducts);
      for(let i=0;i<this.newCollectionProducts.length;i++){
        this.newCollectionProducts[i]["place"]=0;
        this.newCollectionProducts1[i]["place"]=0;
      }
      
    });
  }
  @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
          this.screenHeight = window.innerHeight;
          this.screenWidth = window.innerWidth;
          if(this.screenWidth<=991){
            this.next=1;
          }
          if(this.screenWidth>991){
            this.next=3;
          }
          console.log(this.screenHeight, this.screenWidth);
    }  
}