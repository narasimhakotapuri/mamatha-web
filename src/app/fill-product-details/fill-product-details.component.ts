import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { VisitingCardOrderDetails } from "src/app/_models";
import { VisitingCardOrderDetailsService } from "src/app/_services/visitingcardorderdetails.service";
import { first } from "rxjs/internal/operators/first";
import { AlertService } from "src/app/_services";
import { Router } from "@angular/router";
import { ThrowStmt } from '@angular/compiler';
import { observable } from 'rxjs';

@Component({
  selector: 'app-fill-product-details',
  templateUrl: './fill-product-details.component.html',
  styleUrls: ['./fill-product-details.component.css']
})
export class FillProductDetailsComponent implements OnInit {

  private visitingCardOrderDetails: VisitingCardOrderDetails = new VisitingCardOrderDetails();
  loading = false;
  constructor( 
    private visitingCardOrderDetailsService: VisitingCardOrderDetailsService,
    private router: Router, 
    private alertService: AlertService) {
   }

  ngOnInit() {
  }
  public addVisitingCardOrderDetails() {
    this.visitingCardOrderDetails.address="Address1";
    this.visitingCardOrderDetails.categoryId="5bd20e10cfa2172194cde8fb";
    this.visitingCardOrderDetails.productId="4cd20e10cfa2172194cde8fb";
    this.visitingCardOrderDetails.email="rajesh.das85@gmail.com";
    this.visitingCardOrderDetails.name="rajesh Kumar Das";
    this.visitingCardOrderDetails.comapanyLogoImgBase64Data="rajesh Kumar Das";
    this.visitingCardOrderDetails.comapanyLogoImgName="rajesh";
    this.visitingCardOrderDetails.paperType="Glossy";

    this.loading = true;
    this.visitingCardOrderDetailsService.addVisitingCardOrderDetails(this.visitingCardOrderDetails).pipe(first()).subscribe(
      data => {
        this.alertService.success('visitingCardOrderDetails added successful', true);
        this.router.navigate(['/productDetails']);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }
}
