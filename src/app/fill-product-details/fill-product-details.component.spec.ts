import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FillProductDetailsComponent } from './fill-product-details.component';

describe('FillProductDetailsComponent', () => {
  let component: FillProductDetailsComponent;
  let fixture: ComponentFixture<FillProductDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillProductDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FillProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
