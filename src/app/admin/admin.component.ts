import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  private _opened: boolean = false;

  dis= false;

  className="main-panel side_bar";
  constructor( ) { }

  ngOnInit() {
  }
 
  private _toggleSidebar() {
    this._opened = !this._opened;
  }
  open(f){
    if(this._opened){
      this.className="main-panel side_bar";
      this._opened=true;
      console.log( "close");
      f.close();
    }
    else{
      this.className="main-panel";
      this._opened=false;
      console.log( "open");
      f.open();
    }
  }
  orderprogress(){
    this.dis=!this.dis;
    console.log(this.dis);
    
    
  
  }
}
