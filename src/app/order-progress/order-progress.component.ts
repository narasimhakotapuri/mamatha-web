import { Component, OnInit } from '@angular/core';
import { UserOrderDetails, Addresses } from "src/app/_models/user-order-details";
import { UserorderdetailsService } from "src/app/_services/userorderdetails.service";
import { first } from 'rxjs/operators';
import { AlertService } from "src/app/_services";
//import { UserOrderDetails } from '../_models';
@Component({
  selector: 'app-order-progress',
  templateUrl: './order-progress.component.html',
  styleUrls: ['./order-progress.component.css']
})
export class OrderProgressComponent implements OnInit {
  loading = false;
  UserOrderDetails: UserOrderDetails = new UserOrderDetails();

  private userOrderDetails: UserOrderDetails = new UserOrderDetails();
  private addresses: Addresses = new Addresses();

  arrProductInCartIds = [];
  currentUser: any;
  addressData = {
    firstName: "",
    lastName: "",
    company: "",
    pincode: 0,
    addressLine1: "",
    addressLine2: "",
    city: "",
    state: "",
    country: "",
    phone: "",
    orderMode: "",
  };

  arraddressData = [];
  constructor(private userorderdetailsService: UserorderdetailsService,
    private alertService: AlertService
  ) { }

  ngOnInit() {

    if (localStorage.getItem("productInCartIds")) {
      this.arrProductInCartIds.push(localStorage.getItem("productInCartIds").split(","));
    }
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }
  SaveUserOderDetails() {

    // this.arrProductInCartIds.push("123456");
    // this.arrProductInCartIds.push("699898");

    this.addressData.addressLine1 = "zxcxz";
    this.addressData.pincode = 53655;
    this.addressData.firstName = "sadsa";
    this.arraddressData.push(this.addressData);

    this.addressData.addressLine1 = "ABCD";
    this.addressData.pincode = 9798979;
    this.addressData.firstName = "rfasdfshad";

    this.arraddressData.push(this.addressData);


    this.userOrderDetails.email = this.currentUser.email;
    this.userOrderDetails.paymentMode = "creditcard";
    this.userOrderDetails.addresses = this.arraddressData;

    this.userOrderDetails.productInCartIds = this.arrProductInCartIds;

    this.userorderdetailsService.addUserOrderDetails(this.userOrderDetails).pipe(first()).subscribe(
      data => {
        this.alertService.success('Address added successful', true);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }

}
