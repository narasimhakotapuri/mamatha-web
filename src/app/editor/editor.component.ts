import { Component, OnInit,HostListener } from "@angular/core";
import { CategoryService, AlertService } from "../_services";
import { first } from 'rxjs/operators';
import { ProductService } from "../_services/product.service";
import { Router } from "@angular/router";
import { ProductInCart } from "src/app/_models/ProductInCart";

@Component({
    selector:'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
    productDetails: any;
    productDetails1: any;
    frontimage:any;
    backimage:any;
    image:any;
    categoryList: any;
    categoryList1: any;
    next=3;
    prev=0;
    screenHeight:any;
    screenWidth:any;
    imgSource :any;
    productAdded: boolean = false;
    cartProducts: any;
    loading = false;
    currentUser:any;
    constructor(private alertService: AlertService,private categoryService: CategoryService,private productService:ProductService,private router:Router){}
    ngOnInit(){
        this.categoryService.getAll().pipe(first()).subscribe(products => {
            this.categoryList = products;
            // console.log(this.categoryList[0]);
        });
        // this.productDetails = JSON.parse(localStorage.getItem('productId'));
        this.productDetails = JSON.parse(localStorage.getItem('products'));
        this.productDetails1 = JSON.parse(localStorage.getItem('productdetails'));
        console.log("Naraismha"+this.productDetails1.paper_stock);
        this.productService.getByCategoryId(this.productDetails.sub_category_id).pipe(first()).subscribe(products => {
            // this.productImg = products;
            this.categoryList1=Object.assign([],products);
            console.log(this.categoryList1);
          });
        // console.log(this.productDetails.sub_category_id);
        this.frontimage=this.productDetails.card_images[0].images[0];
        this.backimage=this.productDetails.card_images[0].images[1];
        this.image=this.frontimage;
    }
    front(){
        console.log("front-view");
        this.image=this.frontimage;
    }
    back(){
        console.log("back-view");
        this.image=this.backimage;
    }
    backward(){
        if(this.prev>0){
          this.next=this.next-1;
          this.prev=this.prev-1;
          console.log(this.next +"  "+this.prev);
          // this.i=0;
        }
        console.log("back");
        
      }
      forward(){
        if(this.categoryList1.length-1>this.next){
          this.next=this.next+1;
          this.prev=this.prev+1;
          // this.i=0;
          // if(this.next==5);
          console.log(this.next +"  "+this.prev);
        }
        console.log("front");
        
      }
      @HostListener('window:resize', ['$event'])
        getScreenSize(event?) {
              this.screenHeight = window.innerHeight;
              this.screenWidth = window.innerWidth;
              if(this.screenWidth<=991){
                this.next=0;
              }
              if(this.screenWidth>991){
                this.next=3;
              }
              console.log(this.screenHeight, this.screenWidth);
        }
    quickView(product) {
        //console.log('Index', product);
        localStorage.setItem('productId', JSON.stringify(product));
        this.router.navigate(['/quickView']);
        }
    rasterize(){
      console.log("Narasimha K");
      
    }
    addToCart() {
      // let product = this.productImg[index];
      
      let cartData = [];
      let data = localStorage.getItem('cart');
      
      if (data) {
        cartData = JSON.parse(data);
      }
      console.log(this.productDetails);
      this.productDetails.card_images[0].images[0]=this.imgSource;
      // this.productDetails.imgData.imgBase64Data = this.imgSource;
      this.updateCartData(this.productDetails);
      this.productAdded = true;
     // location.reload();
    }
    updateCartData(cartData) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      let  productInCart  = new ProductInCart();  
      this.cartProducts = cartData;
      productInCart.categoryId = this.cartProducts.categoryId;
      productInCart.productInCartId = this.cartProducts.id;
      productInCart.desc = this.cartProducts.desc;
      productInCart.imgName = this.cartProducts.imgName;
      // productInCart.imgBase64Data = this.cartProducts.imgData.imgBase64Data;
      productInCart.price = this.cartProducts.price;
      if(this.currentUser){
        productInCart.email = this.currentUser.email;
      }
      this.loading = true;
      this.productService.addProductInCart(productInCart).pipe(first()).subscribe(
        data => {
           localStorage.removeItem('categoryId');
          localStorage.removeItem('productId');
          this.alertService.success('productInCart added successful', true);
          this.goToCart();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
      } 
    
    goToCart() {
      this.router
        .navigate(['/cart']);
    }
}