import { Component, OnInit ,HostListener } from '@angular/core';
import { Product, VisitingCardOrderDetails } from '../_models';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { VisitingCardOrderDetailsService, AlertService } from '../_services';
import { ProductInCart } from "src/app/_models/ProductInCart";
import { ProductService } from "src/app/_services/product.service";
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-product-quickview',
  templateUrl: './product-quickview.component.html',
  styleUrls: ['./product-quickview.component.css']
})
export class ProductQuickviewComponent implements OnInit {
  cartProducts: any;
 // cartProductLength: any;
  products: Product[] = [];
  myThumbnail="";
  myFullresImage="";
  productDetails: any;
  relativeProducts :any;
  imgSource :any;
  screenHeight:any;
  screenWidth:any;
  comapanyLogoImgBase64Data: any;
  addToCartDisableForProductDetails: boolean = false;
  index : number;
  data = {
    name: '',
    email: '',
    address: '',
    comapanyLogoImgName: '',
    paperType: '',
    categoryId: '',
    productId: '',
    comapanyLogoImgBase64Data: ''
  };
  url="http://jasonwatmore.coms";
  productAdded: boolean = false;
  urls = [];
  cataguryId: any;
  loading = false;
  private visitingCardOrderDetails: VisitingCardOrderDetails = new VisitingCardOrderDetails();
  imgName: any;
  currentUser:any;
  errorMsg: string;
  imagePresent="";
  currentImage='';
  currentImage1='';
  imgBase64Data: string | ArrayBuffer;
  next=3;
  prev=0;
  quant=50;
  simha="Velvet touch";
  simha1="Standard"
  constructor(private router: Router, 
    private visitingCardOrderDetailsService: VisitingCardOrderDetailsService,
    private alertService: AlertService,
    private productService: ProductService) 
  { 
  
    this.getScreenSize();
  }

  ngOnInit() {
    
    this.loadAllProducts();
    let data = localStorage.getItem('cart');
    if (data) {
      this.cartProducts = JSON.parse(data);
      //this.cartProductLength = this.cartProducts.length;
    } else {
      this.cartProducts = [];
      //this.cartProductLength = this.cartProducts.length;
    }
  }
  private loadAllProducts() {
    this.productDetails = JSON.parse(localStorage.getItem('productId'));
    // console.log(this.productDetails);
    this.imagePresent="https://mamata-card-images.s3.us-east-2.amazonaws.com/"+this.productDetails.card_images[0].images[0];    
    this.currentImage=this.productDetails.card_images[0].images[0];
    this.currentImage1=this.productDetails.card_images[0].images[1];
    this.data.categoryId = this.productDetails.categoryId;
    this.data.productId = this.productDetails.id;
    this.productService.getAllNewCollections().pipe(first()).subscribe(newCallectionsproducts => {
      this.relativeProducts=Object.assign([],newCallectionsproducts);
    });
  } 
  changeImg(colorCode,index){
    this.currentImage=this.productDetails.card_images[index].images[0];
    this.currentImage1=this.productDetails.card_images[index].images[1];
    this.imagePresent="https://mamata-card-images.s3.us-east-2.amazonaws.com/"+this.currentImage;
  }
  position(img){
    this.imagePresent="https://mamata-card-images.s3.us-east-2.amazonaws.com/"+img;
  }
  addToCart() {
    // let product = this.productImg[index];
    let cartData = [];
    let data = localStorage.getItem('cart');
    if (data) {
      cartData = JSON.parse(data);
    }
    this.productDetails.imgData.imgBase64Data = this.imgSource;
    //cartData.push(this.productDetails);
    // this.updateCartData(cartData);
   // localStorage.setItem('cart', JSON.stringify(cartData));
    this.updateCartData(this.productDetails);
    this.productAdded = true;
   // location.reload();
  }
  updateCartData(cartData) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let  productInCart  = new ProductInCart();  
    this.cartProducts = cartData;
    productInCart.categoryId = this.cartProducts.categoryId;
    productInCart.productInCartId = this.cartProducts.id;
    productInCart.desc = this.cartProducts.desc;
    productInCart.imgName = this.cartProducts.imgName;
    productInCart.imgBase64Data = this.cartProducts.imgData.imgBase64Data;
    productInCart.price = this.cartProducts.price;
    if(this.currentUser){
      productInCart.email = this.currentUser.email;
    }
    this.loading = true;
    this.productService.addProductInCart(productInCart).pipe(first()).subscribe(
      data => {
         localStorage.removeItem('categoryId');
         localStorage.removeItem('productId');
        this.alertService.success('productInCart added successful', true);
        this.goToCart();
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
    } 
  
  goToCart() {
    this.router
      .navigate(['/cart']);
  }
  SendProductDetails() {
    this.visitingCardOrderDetails.address = this.data.address;
    this.visitingCardOrderDetails.categoryId = this.data.categoryId;
    this.visitingCardOrderDetails.productId = this.data.productId;
    this.visitingCardOrderDetails.email = this.data.email;
    this.visitingCardOrderDetails.name = this.data.name;
    this.visitingCardOrderDetails.comapanyLogoImgBase64Data = this.comapanyLogoImgBase64Data;
    this.visitingCardOrderDetails.comapanyLogoImgName = this.data.comapanyLogoImgName;
    this.visitingCardOrderDetails.paperType = this.data.paperType;

    this.loading = true;

    this.visitingCardOrderDetailsService.addVisitingCardOrderDetails(this.visitingCardOrderDetails).pipe(first()).subscribe(
      data => {
        this.addToCart();
        this.alertService.success('visitingCardOrderDetails added successful', true);
       // this.addToCartDisableForProductDetails = true;
        

        // this.router.navigate(['/productDetails']);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var idxDot = file.type.lastIndexOf(".") + 1;
    var extFile = file.type.substr(idxDot, file.type.length).toLowerCase();
    if (extFile == "image/jpg" || extFile == "image/jpeg" || extFile == "image/png") {
      if (file.size <= 560000) {
        this.errorMsg = '';
        this.data.comapanyLogoImgName = file.name;
        var myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
          this.comapanyLogoImgBase64Data = myReader.result;
        }
        myReader.readAsDataURL(file);
      } else {
        this.errorMsg = 'Image size should be less than 70kb';
      }
    } else {
      this.errorMsg = 'Only jpg/jpeg and png files are allowed!';
    }
  }
  back(){
    if(this.prev>0){
      this.next=this.next-1;
      this.prev=this.prev-1;
      console.log(this.next +"  "+this.prev);
      // this.i=0;
    }
    console.log("back");
    
  }
  front(){
    if(this.relativeProducts.length-1>this.next){
      this.next=this.next+1;
      this.prev=this.prev+1;
      // this.i=0;
      // if(this.next==5);
      console.log(this.next +"  "+this.prev);
    }
    console.log("front");
    
  }
  @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
          this.screenHeight = window.innerHeight;
          this.screenWidth = window.innerWidth;
          if(this.screenWidth<=991){
            this.next=0;
          }
          if(this.screenWidth>991){
            this.next=3;
          }
          console.log(this.screenHeight, this.screenWidth);
    }
  quickView(product) {
    console.log('Index'+product.description);
    localStorage.setItem('productId', JSON.stringify(product));
    this.loadAllProducts();
  }
  editing(product,f:NgForm){
    // console.log(f.value);
    let values1:any;
    values1={
      paper_stock:f.value.paper_stock,
      paper_quality:f.value.paper_quality,
      price:this.productDetails.actual_price* (this.quant/50)
    }
    // console.log(values1);
    localStorage.setItem("productdetails",JSON.stringify(values1));
    localStorage.setItem('products', JSON.stringify(product));
    this.router.navigate(['/edit']);
  }
  increase(){
    if(this.quant<1000)
      this.quant+=50;
  }
  decrease(){
    if(this.quant>50)
      this.quant-=50;
  }
  
}
