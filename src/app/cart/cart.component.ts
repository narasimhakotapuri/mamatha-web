import { Component, OnInit } from '@angular/core';
import { DEFAULT_INTERPOLATION_CONFIG } from '@angular/compiler/src/ml_parser/interpolation_config';
import { Router } from '@angular/router';
import { ProductService } from "src/app/_services/product.service";
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartProducts: any;
  bill: any;
  currentUser: any;
  public productInCartIds : any;
  constructor(private router: Router,
    private productService: ProductService) {

  }

  ngOnInit() {
    this.initiateData();
  }

  initiateData() {
    // let data ;
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser) {
      this.productService.getAllProductInCartByEmail(this.currentUser.email).pipe(first()).subscribe(products => {
        this.cartProducts = products;
        this.bill = 0;
        if (this.cartProducts) {
          this.productInCartIds = [];
           localStorage.removeItem("productInCartIds");
          //this.cartProducts = JSON.parse(data);
          for (let i in this.cartProducts) {
            this.cartProducts[i]["qty"] = 1;
            this.bill = this.bill + this.cartProducts[i].price * this.cartProducts[i].qty;
            this.productInCartIds.push(this.cartProducts[i]["productInCartId"]);
             console.log(this.productInCartIds);
          }
        } else {
          this.cartProducts = [];
        }

      });
    }
  }
  goBackToHomePage() {
    this.router.navigate(['/home']);
  }
  goToOrderProgressPage() {
    localStorage.setItem("productInCartIds", this.productInCartIds)
    this.router.navigate(['/orderProgress']);
  }
  updateTotal() {
    this.bill = 0;
    for (let i in this.cartProducts) {
      this.bill = this.bill + this.cartProducts[i].price * this.cartProducts[i].qty;
    }
  }

  removeItem(id) {
    localStorage.setItem('rowIdRemove', id);
    this.productService.deleteCartData(this.currentUser.email).pipe(first()).subscribe(products => {

    });
  }
  removeItemRow() {
    let idToRemove = localStorage.getItem('rowIdRemove');
    this.productService.deleteCartData(idToRemove).pipe(first()).subscribe(products => {
      localStorage.removeItem('rowIdRemove');
    });
    // this.cartProducts.splice(idRemove, 1);
    // if (this.cartProducts.length) {
    //   localStorage.setItem('cart', JSON.stringify(this.cartProducts));
    // } else {
    //   this.cartProducts = [];
    //   localStorage.setItem('cart', this.cartProducts);
    // }
    location.reload();
  }

  payBill() {
    if (this.cartProducts.length) {
      localStorage.removeItem('cart');
      this.initiateData();
      alert("Your bill is: " + this.bill);
    } else {
      alert("No items in cart");
    }
  }
}
