const express = require('express');
const port = process.env.PORT || 3000;
const app = express();

const path=require('path');

app.use(express.static(__dirname+'../dist'));
// console.log(dist);
// window.alert(dist)

// app.use(express.static(path.join(__dirname, 'dist')));

app.get('*',function(req,res){
    res.sendFile(path.join(__dirname,'src','index.html'));
});
app.listen(port);
// app.set('port', port);
// var server = app.listen(port);
// console.log('listening on port 4000');